<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'home'])->name('home');

Route::get('/users/register', [\App\Http\Controllers\UsersController::class, 'register'])->name('users.register');

Route::post('users', [\App\Http\Controllers\UsersController::class, 'store'])->name('users.store');

Route::get('/login', [\App\Http\Controllers\SessionsController::class, 'create'])->name('sessions.login');
Route::post('login', [\App\Http\Controllers\SessionsController::class, 'store'])->name('sessions.store');
Route::delete('/logout', [\App\Http\Controllers\SessionsController::class, 'destroy'])->name('sessions.delete');

Route::resource('genres', \App\Http\Controllers\GenresController::class)->only(['index', 'show']);

Route::resource('libCards', \App\Http\Controllers\LibCardsCotroller::class)->only(['show']);

Route::resource('items', \App\Http\Controllers\ItemsController::class)->only(['create', 'store']);

Route::put('items/{item}', [\App\Http\Controllers\ItemsController::class, 'returnBook'])->name('items.return');
