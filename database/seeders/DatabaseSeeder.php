<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\Genre;
use App\Models\Item;
use App\Models\Lib_card;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::factory()->count(10)->has(Lib_card::factory())->create();
        Genre::factory()->count(10)->create();
        Book::factory()->count(200)->create();
        Item::factory()->count(100)->create();
    }
}
