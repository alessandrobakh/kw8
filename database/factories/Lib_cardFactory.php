<?php

namespace Database\Factories;

use App\Models\Lib_card;
use Illuminate\Database\Eloquent\Factories\Factory;

class Lib_cardFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lib_card::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->isbn13()
        ];
    }
}
