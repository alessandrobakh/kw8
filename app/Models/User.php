<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class User extends Model
{
    use HasFactory;

    protected $fillable = ['full_name', 'address', 'passport', 'password'];

    /**
     * @return HasOne
     */
    public function libCard()
    {
        return $this->hasOne(Lib_card::class);
    }
}
