<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Item extends Model
{
    use HasFactory;

    protected $fillable = ['lib_card_id', 'book_id', 'return_date', 'return_book'];

    /**
     * @return BelongsTo
     */
    public function libCard()
    {
        return $this->belongsTo(Lib_card::class);
    }

    /**
     * @return BelongsTo
     */
    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
