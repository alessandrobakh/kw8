<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Item;
use App\Models\Lib_card;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ItemsController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Factory|View|\Illuminate\Http\RedirectResponse|Response
     */
    public function create(Request $request)
    {
        if (!session()->exists('lib_card_name')) {
            return redirect()->route('home');
        }
        $book = Book::find($request->input('book_id'));
        return view('items.create', compact('book'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|Response
     */
    public function store(Request $request)
    {
        $item = new Item($request->all());
        $item->save();
        return redirect()->route('home')->with('success', 'Book added!');
    }

    /**
     * @param Request $request
     * @param Item $item
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnBook(Request $request, Item $item)
    {
        $item->update($request->all());
        $item->return_book = true;
        $item->save();
        return redirect()->back();
    }
}
