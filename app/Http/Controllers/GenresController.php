<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GenresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|Response
     */
    public function index()
    {
        $genres = Genre::paginate(1);
        return view('genres.index', compact('genres'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Genre  $genre
     * @return Factory|View|Response
     */
    public function show(Genre $genre)
    {
        return view('genres.show', compact('genre'));
    }
}
