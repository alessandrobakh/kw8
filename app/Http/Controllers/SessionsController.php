<?php

namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SessionsController extends AuthController
{
    /**
     * @param Request $request
     * @return Factory|View|RedirectResponse
     */
    public function create(Request $request)
    {
        if ($request->session()->exists('lib_card_name')) {
            return redirect()->route('home')->with('error', 'You are already login!');
        }
        return view('sessions.create');
    }

    /**
     * @param SessionRequest $request
     * @return RedirectResponse
     */
    public function store(SessionRequest $request)
    {
        $user_collection = User::where('passport', $request->get('passport'))->get();

        if ($user_collection->isNotEmpty()){
            $user = $user_collection->first();
            if ($this->auth($user, $request->get('password'))){
                $this->logIn($user);
                return redirect()->route('home')->with('success', 'You are successfully log in');
            }
        }
        return redirect()->back()->with('error', 'Incorrect email or password!');
    }

    /**
     * @return RedirectResponse
     */
    public function destroy()
    {
        $this->logOut();
        return redirect()->route('sessions.login')->with('success', 'You are successfully log out');
    }
}
