<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @param User $user
     * @param $password
     * @return bool
     */
    protected function auth(User $user, $password)
    {
        return Hash::check($password, $user->password);
    }

    /**
     * @param User $user
     * @return void
     */
    protected function logIn(User $user)
    {
        session()->put('lib_card_name', $user->libCard->name);
    }

    /**
     *@return void
     */
    protected function logOut()
    {
        session()->remove('lib_card_name');
        session()->regenerate();
    }
}
