<?php

namespace App\Http\Controllers;

use App\Models\Lib_card;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LibCardsCotroller extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param Lib_card $libCard
     * @return Factory|View|\Illuminate\Http\RedirectResponse|Response
     */
    public function show(Lib_card $libCard)
    {
        if (!session()->exists('lib_card_name')) {
            return redirect()->route('home');
        }
        return view('libCards.show', compact('libCard'));
    }
}
