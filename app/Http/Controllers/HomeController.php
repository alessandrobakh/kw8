<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @return Factory|View|RedirectResponse
     */
    public function home()
    {
        if (session()->exists('lib_card_name')){
            return view('layouts.home');
        }
        return view('sessions.create');
    }
}
