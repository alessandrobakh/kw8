<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\Lib_card;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends AuthController
{
    /**
     * @param Request $request
     * @return Factory|View|RedirectResponse
     */
    public function register(Request $request)
    {
        if ($request->session()->exists('lib_card_name')){
            return redirect()->route('home')->with('error', 'You are already registered!');
        }
        return view('users.register');
    }

    /**
     * @param RegisterRequest $request
     * @return RedirectResponse
     */
    public function store(RegisterRequest $request)
    {
        $payload = $request->all();
        $payload['password'] = Hash::make($payload['password']);
        $user = User::create($payload);

        $now = Carbon::now();
        $lib_card_name = $now->format('YmdHisu');

        $lib_card = Lib_card::create(['user_id' => $user->id, 'name' => $lib_card_name]);
        $this->logIn($user);
        return redirect()->route('home')->with('success', 'You are successfully registered!');
    }
}
