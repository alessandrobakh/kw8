@extends('layouts.base')

@section('content')

    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Register</h1>
            <hr>
            <form action="{{route('users.store')}}" method="post">
                @csrf

                <div class="form-group">
                    <label for="full_name">Full Name:</label>
                    <input type="text" class="form-control" id="full_name" name="full_name">
                </div>

                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" id="address" name="address">
                </div>

                <div class="form-group">
                    <label for="passport">Passport ID:</label>
                    <input type="text" class="form-control" id="passport" aria-describedby="passport-h" name="passport">
                    <small id="passport-h" class="form-text text-muted">We'll never share your passport with anyone else.</small>
                </div>

                <div class="form-group">
                    <label for="password1">Password</label>
                    <input type="password" class="form-control" id="password1" name="password">
                </div>
                <div class="form-group">
                    <label for="password2">Confirm password</label>
                    <input type="password" class="form-control" id="password2" name="password_confirmation">
                </div>

                <button type="submit" class="btn btn-outline-primary">Get library Card</button>
            </form>
        </div>
    </div>
@endsection
