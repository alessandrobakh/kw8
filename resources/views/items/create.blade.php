@extends('layouts.base')

@section('content')

    @include('notifications.alerts')


    <form action="{{route('items.store')}}" method="post">
        @csrf

        <input type="hidden" name="lib_card_id" value="{{\App\Models\Lib_card::where('name', session()->get('lib_card_name'))->first()->id}}">

        <div class="form-group">
            <label for="book_id">Book:</label>
        <select class="form-control" name="book_id" id="book_id">
            <option class="form-control" selected value="{{$book->id}}">{{$book->author}} {{$book->title}}</option>
        </select>
        </div>

        <div class="form-group">
        <label for="return_date">Return date</label>
        <input class="form-control" type="date" name="return_date" id="return_date">
        </div>

        <button type="submit" class="btn btn-primary">Take book</button>

    </form>

@endsection
