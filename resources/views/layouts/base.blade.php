<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Library</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            Library
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="dropdown show">
            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Genres of Books
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{route('genres.index')}}">All</a>
                @foreach(\App\Models\Genre::all() as $genre)
                <a class="dropdown-item" href="{{route('genres.show', ['genre' => $genre])}}">{{$genre->name}}</a>
                @endforeach
            </div>
        </div>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @if(session()->exists('lib_card_name'))
                <li class="nav-item">
                    <a class="btn-outline-primary btn nav-link" href="{{route('libCards.show', ['libCard' => \App\Models\Lib_card::where('name', session()->get('lib_card_name'))->first()->id])}}">My Books</a>
                </li>
                @endif
            </ul>

            <ul class="navbar-nav ml-auto">
                @if(session()->exists('lib_card_name'))

                    <form action="{{route('sessions.delete')}}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-sm btn-outline-danger">LOGOUT</button>
                    </form>

                @else
                <li class="nav-item">
                    <a class="btn-primary btn nav-link" href="{{route('sessions.login')}}">Log in</a>
                </li>
                <li class="nav-item">
                    <a class="btn-outline-success btn nav-link" href="{{route('users.register')}}">Register</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

    <div class="container">
        @yield('content')
    </div>

</body>
</html>
