@extends('layouts.base')

@section('content')

    <h1>Books of genre "{{$genre->name}}": </h1>

    @if($genre->books->count()>0)

            <div class="row row-cols-1 row-cols-md-3 g-4">
                @foreach($genre->books as $book)

                    <div class="col mb-1">
                        <div class="card" style="max-width: 200px">
                            <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top" alt="{{asset('/storage/' . $book->picture)}}">
                            <div class="card-body">
                                <h5 class="card-title">{{$book->author}}</h5>
                                <p class="card-text">{{$book->title}}</p>
                                @if(session()->exists('lib_card_name'))
                                    <form action="{{route('items.create')}}" method="post">
                                        @csrf
                                        @method('get')
                                        <input type="hidden" name="book_id" value="{{$book->id}}">
                                        <button class="btn btn-outline-primary">Order</button>
                                    </form>
                                @endif
                            </div>
                        </div>

                    </div>

                @endforeach
            </div>

    @else

        <p>no genres</p>

    @endif

@endsection
