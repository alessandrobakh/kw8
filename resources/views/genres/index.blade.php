@extends('layouts.base')

@section('content')

    <h1>Books of all genres: </h1>

    @if($genres->count()>0)

        @foreach($genres as $genre)

            <h4>{{$genre->name}}</h4>

            <div class="row row-cols-1 row-cols-md-3 g-4">
            @foreach($genre->books as $book)

                    <div class="col mb-1">
                        <div class="card" style="max-width: 200px">
                            <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top" alt="{{asset('/storage/' . $book->picture)}}">
                            <div class="card-body">
                                <h5 class="card-title">{{$book->author}}</h5>
                                <p class="card-text">{{$book->title}}</p>
                                @if(session()->exists('lib_card_name'))
                                    @if(is_null(\App\Models\Lib_card::where('name', session()->get('lib_card_name'))->first()->items->where('book_id', $book->id)->first()) or
                                        \App\Models\Lib_card::where('name', session()->get('lib_card_name'))->first()->items->where('book_id', $book->id)->first()->return_book == true)

                                    <form action="{{route('items.create')}}" method="post">
                                        @csrf
                                        @method('get')
                                        <input type="hidden" name="book_id" value="{{$book->id}}">
                                        <button class="btn btn-outline-primary">Order</button>
                                    </form>
                                    @endif
                                @endif
                            </div>
                        </div>

                    </div>

            @endforeach
            </div>

        @endforeach

    @else

        <p>no genres</p>

    @endif

    <div class="row justify-content-md-center p-5">

        <div class="col-md-auto">

            {{ $genres->links('pagination::bootstrap-4') }}

        </div>

    </div>

@endsection
