@extends('layouts.base')

@section('content')

    <h1>{{ucfirst($libCard->user->full_name)}}, your books:</h1>

    @if ($libCard->items->count()>0)

            <table class="table">
                <thead>
                <tr>
                    <th>Author</th>
                    <th>Book</th>
                    <th>Return date</th>
                    <th>Returned</th>
                </tr>
                </thead>
                <tbody>
                @foreach($libCard->items as $item)
                    <tr>
                        <td>{{$item->book->author}}</td>
                        <td>{{$item->book->title}}</td>
                        <td>{{$item->return_date}}</td>
                        @if($item->return_book == true)
                        <td>Returned</td>
                        @else
                         <td>Not Returned <form class="float-right" action="{{route('items.return', ['item' => $item->id])}}" method="post">
                                 @csrf
                                 @method('put')
                                 <input type="hidden" name="lib_card_id" value="{{\App\Models\Lib_card::where('name', session()->get('lib_card_name'))->first()->id}}">
                                 <input type="hidden" name="book_id" value="{{$item->book->id}}">
                                 <input type="hidden" name="return_date" value="{{$item->return_date}}">

                                 <button class="btn btn-outline-primary" type="submit">Return</button>
                             </form></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>

    @else

    <p>no books</p>

    @endif

@endsection
